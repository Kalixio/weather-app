/**
 * @file Log.cpp
 * @author Romain BOULLARD 
 * @date 2021-11-20
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "Debug/Log/Log.h"

#include "spdlog/sinks/stdout_color_sinks.h"

namespace Weather
{
    Log::Log()
    {
        spdlog::set_pattern("%^[%T] %n: %v%$");

        m_logger = spdlog::stdout_color_mt("WEATHER");
		m_logger->set_level(spdlog::level::trace);
    }

    /****************************************************************************************/

    Log* Log::GetInstance()
    {
        if(!m_instance)
        {
            m_instance = std::make_unique<Log>();
        }

        return m_instance.get();
    }
}