/**
 * @file Log.h
 * @author Romain BOULLARD 
 * @date 2021-11-20
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include "Utils/Expected/Expected.h"

#include "spdlog/spdlog.h"

#include <string>
#include <memory>

namespace Weather
{
    /**
     * A logging class.
     * A lazy singleton (bad, but will do. It's just a logger).
     */
    class Log
    {
        public:
            /**
             * Constructor
             */
            Log();

            /**
             * Destructor
             */
            ~Log() = default;

            /**
             * Get the instance of Log. Or create one if not created yet.
             * 
             * @return const Log* The instance of Log
             */
            static Log* GetInstance();

            /**
             * Log an info level message.
             * 
             * @tparam FormatString
             * @tparam Args
             * @param p_fmt 
             * @param p_args 
             */
            template<typename FormatString, typename... Args>
		    void LogInfo(const FormatString& p_fmt, Args&&... p_args);

            /**
             * Log a warn level message.
             * 
             * @tparam FormatString 
             * @tparam Args 
             * @param p_fmt 
             * @param p_args 
             */
            template<typename FormatString, typename... Args>
		    void LogWarn(const FormatString& p_fmt, Args&&... p_args);

            /**
             * Log an error level message.
             * 
             * @tparam FormatString 
             * @tparam Args 
             * @param p_fmt 
             * @param p_args 
             */
            template<typename FormatString, typename... Args>
		    void LogError(const FormatString& p_fmt, Args&&... p_args);

            /**
             * Log a trace level message.
             * 
             * @tparam FormatString 
             * @tparam Args 
             * @param p_fmt 
             * @param p_args 
             */
            template<typename FormatString, typename... Args>
		    void LogTrace(const FormatString& p_fmt, Args&&... p_args);
        private:
            /**
             * The instance of Log.
             */
            inline static std::unique_ptr<Log> m_instance = nullptr;

            /**
             * The logger
             */
            std::shared_ptr<spdlog::logger> m_logger = nullptr;
    };

    /****************************************************************************************/

	template<typename FormatString, typename... Args>
	inline void Log::LogInfo(const FormatString& p_fmt, Args&&... p_args)
	{
		m_logger->info(p_fmt, std::forward<Args>(p_args)...);
	}

    /****************************************************************************************/

	template<typename FormatString, typename... Args>
	inline void Log::LogWarn(const FormatString& p_fmt, Args&&... p_args)
	{
		m_logger->warn(p_fmt, std::forward<Args>(p_args)...);
	}

    /****************************************************************************************/

	template<typename FormatString, typename... Args>
	inline void Log::LogError(const FormatString& p_fmt, Args&&... p_args)
	{
		m_logger->error(p_fmt, std::forward<Args>(p_args)...);
	}

    /****************************************************************************************/

	template<typename FormatString, typename... Args>
	inline void Log::LogTrace(const FormatString& p_fmt, Args&&... p_args)
	{
		m_logger->trace(p_fmt, std::forward<Args>(p_args)...);
	}
}

/****************************************************************************************/

#if defined WEATHER_DEBUG || defined WEATHER_RELEASE_WITH_DEBUG_INFO
#define LOG_INFO(...)	Weather::Log::GetInstance()->LogInfo(__VA_ARGS__)
#define LOG_WARN(...)	Weather::Log::GetInstance()->LogWarn(__VA_ARGS__)
#define LOG_ERROR(...)	Weather::Log::GetInstance()->LogError(__VA_ARGS__)
#define LOG_TRACE(...)	Weather::Log::GetInstance()->LogTrace(__VA_ARGS__)
#else
#define LOG_INFO(...)
#define LOG_WARN(...)
#define LOG_ERROR(...)
#define LOG_TRACE(...)
#endif
