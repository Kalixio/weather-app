/**
 * @file Date.h
 * @author Romain BOULLARD 
 * @date 2021-11-21
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include <cstdint>

namespace Weather
{
    struct Date
	{
        /**
         * Constructor
         * 
         * @param p_epoch The EPOCH.
         */
		Date(const std::uint64_t p_epoch);

        /**
         * Destructor
         */
		~Date() = default;

        /**
         * Get the EPOCH.
         * 
         * @return std::uint64_t The EPOCH.
         */
		std::uint64_t GetEpoch() const;

        /**
         * Get the year.
         * 
         * @return std::uint16_t The year.
         */
		std::uint16_t GetYear() const;

        /**
         * Get the month.
         * 
         * @return std::uint8_t The month.
         */
		std::uint8_t GetMonth() const;

        /**
         * Get the day.
         * 
         * @return std::uint8_t The day.
         */
		std::uint8_t GetDay() const;

        /**
         * Get the hour.
         * 
         * @return std::uint8_t The hour.
         */
		std::uint8_t GetHour() const;

        /**
         * Get the minute.
         * 
         * @return std::uint8_t The minute.
         */
		std::uint8_t GetMinute() const;

        /**
         * Get the second.
         * 
         * @return std::uint8_t The second.
         */
		std::uint8_t GetSecond() const;
	private:
		/**
		 * The epoch.
		 */
		std::uint64_t m_epoch;

		/**
		 * The year.
		 */
		std::uint16_t m_year;

		/**
		 * The month.
		 */
		std::uint8_t m_month;

		/**
		 * The day.
		 */
		std::uint8_t m_day;

		/**
		 * The hour.
		 */
		std::uint8_t m_hour;

		/**
		 * The minute.
		 */
		std::uint8_t m_minute;

		/**
		 * The second.
		 */
		std::uint8_t m_second;
	};
}