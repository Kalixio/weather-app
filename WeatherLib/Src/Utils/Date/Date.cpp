/**
 * @file Date.cpp
 * @author Romain BOULLARD 
 * @date 2021-11-21
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "Utils/Date/Date.h"

#include <limits>
#include <algorithm>
#include <chrono>
#include <sstream>
#include <iomanip>


namespace Weather
{
    Date::Date(const std::uint64_t p_epoch) :
		m_epoch(std::clamp(p_epoch, std::numeric_limits<std::uint64_t>().min(), std::numeric_limits<std::uint64_t>().max()))
	{
		std::stringstream dateStream;
        std::time_t secondsSinceEpoch = m_epoch;
		std::uint16_t year, month, day, hour, minute, second;

		dateStream << std::put_time(std::localtime(&secondsSinceEpoch), "%Y %m %d %H %M %S");

		dateStream >> year >> month >> day >> hour >> minute >> second;

		m_year = static_cast<std::uint16_t>(year);
		m_month = static_cast<std::uint8_t>(month);
		m_day = static_cast<std::uint8_t>(day);
		m_hour = static_cast<std::uint8_t>(hour);
		m_minute = static_cast<std::uint8_t>(minute);
		m_second = static_cast<std::uint8_t>(second);
	}

	/****************************************************************************************/

	std::uint64_t Date::GetEpoch() const
	{
		return m_epoch;
	}

	/****************************************************************************************/

	std::uint16_t Date::GetYear() const
	{
		return m_year;
	}

	/****************************************************************************************/

	std::uint8_t Date::GetMonth() const
	{
		return m_month;
	}

	/****************************************************************************************/

	std::uint8_t Date::GetDay() const
	{
		return m_day;
	}

	/****************************************************************************************/

	std::uint8_t Date::GetHour() const
	{
		return m_hour;
	}

	/****************************************************************************************/

	std::uint8_t Date::GetMinute() const
	{
		return m_minute;
	}

	/****************************************************************************************/

	std::uint8_t Date::GetSecond() const
	{
		return m_second;
	}
}