/**
 * @file Expected.h
 * @author Romain BOULLARD 
 * @date 2021-11-20
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include "tl/expected.hpp"

namespace Weather
{
    template<class TYPE, class ERROR>
    using Expected = tl::expected<TYPE, ERROR>;

    /****************************************************************************************/

    template<class ERROR>
    using Unexpected = tl::unexpected<ERROR>;

    /****************************************************************************************/

    template<class ERROR>
    using Valid = tl::expected<void, ERROR>;
}