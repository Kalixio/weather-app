/**
 * @file Data.h
 * @author Romain BOULLARD 
 * @date 2021-11-21
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include "Utils/Date/Date.h"

#include <optional>

namespace Weather
{
    /**
     * Data about the weather
     */
    struct Data
    {
        /**
         * Date for sunrise.
         */
        std::optional<Date> m_sunrise;
        
        /**
         * Date for sunset
         */
        std::optional<Date> m_sunset;

        /**
         * Temperature (Celsius)
         */
        float m_temp;
        
        /**
         * Temperature feel (Celsius).
         */
        float m_feels;
        
        /**
         * Humidity (percentage)
         */
        std::uint8_t m_humidity;
        
        /**
         * Cloud coverage (percentage)
         */
        std::uint8_t m_clouds;
    };
}