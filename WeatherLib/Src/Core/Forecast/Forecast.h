/**
 * @file Forecast.h
 * @author Romain BOULLARD 
 * @date 2021-11-21
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include "Core/Forecast/Data/Data.h"

#include "Utils/Expected/Expected.h"

#include <string>
#include <array>

#define MAX_FORECAST 5

namespace Weather
{
    /**
     * Status that can be returned by the Forecast methods.
     */
    enum class ForecastStatus
    {
        ForecastStatus_Undefined = 1,       //Undefined error.
        ForecastStatus_LocationNotFound,    //Location not found.
        ForecastStatus_FailedToGetLocation, //Failed to get location.
        ForecastStatus_FailedToGetForecast, //Failed to get forecast.
        ForecastStatus_NotValid             //Not valid.
    };

    /****************************************************************************************/

    /**
     * Convert a ForecastStatus to a human readable string.
     * 
     * @param p_forecastStatus The ForecastStatus to convert.
     * @return std::string The ForecastStatus as a human readable string.
     */
    std::string ForecastStatusToString(const ForecastStatus p_forecastStatus);

    /****************************************************************************************/

    /**
     * To create a Forecast.
     */
    struct ForecastCreateInfo
    {
        /**
         * Location to get the forecast of.
         */
        std::string m_location;
    };

    /****************************************************************************************/

    /**
     * A Forecast.
     */
    class Forecast
    {
        public:
            /**
             * Constructor.
             * 
             * @param p_forecastCreateInfo The ForecastCreateInfo.
             */
            Forecast(const ForecastCreateInfo& p_forecastCreateInfo);

            /**
             * Destructor
             */
            ~Forecast() = default;

            /**
             * Get the current weather.
             * 
             * @return Expected<Data, ForecastStatus> The current weather.
             */
            Expected<Data, ForecastStatus> GetCurrent() const;

            /**
             * Get the hourly weather.
             * 
             * @return Expected<std::array<Data, MAX_FORECAST>, ForecastStatus> The hourly weather.
             */
            Expected<std::array<Data, MAX_FORECAST>, ForecastStatus> GetHourly() const;

            /**
             * Get the daily weather.
             * 
             * @return Expected<std::array<Data, MAX_FORECAST>, ForecastStatus> The daily weather.
             */
            Expected<std::array<Data, MAX_FORECAST>, ForecastStatus> GetDaily() const;

            /**
             * Get the validity status of the forecast.
             * 
             * @return Valid<ForecastStatus> The validity status of the forecast.
             */
            Valid<ForecastStatus> GetValid() const;
        private:
            /**
             * The validity status of the forecast.
             */
            Valid<ForecastStatus> m_valid;

            /**
             * The current weather
             */
            Data m_current;

            /**
             * The hourly weather.
             */
            std::array<Data, MAX_FORECAST> m_hourly;

            /**
             * The daily weather.
             */
            std::array<Data, MAX_FORECAST> m_daily;
    };
}