/**
 * @file Forecast.cpp
 * @author Romain BOULLARD 
 * @date 2021-11-21
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "Core/Forecast/Forecast.h"
#include "Core/Web/Getter/Getter.h"

#include "Debug/Log/Log.h"

#include <sstream>
#include <algorithm>

namespace Weather
{
    static Data GetDataCurrent(const rapidjson::Value& p_value)
    {
        Data data = {
            Date(p_value["sunrise"].GetUint64()),
            Date(p_value["sunset"].GetUint64()),
            p_value["temp"].GetFloat(),
            p_value["feels_like"].GetFloat(),
            static_cast<std::uint8_t>(p_value["humidity"].GetInt()),
            static_cast<std::uint8_t>(p_value["clouds"].GetInt())
        };

        return data;
    }

    /****************************************************************************************/

    static Data GetDataHourly(const rapidjson::Value& p_value)
    {
        Data data = {
            std::nullopt,
            std::nullopt,
            p_value["temp"].GetFloat(),
            p_value["feels_like"].GetFloat(),
            static_cast<std::uint8_t>(p_value["humidity"].GetInt()),
            static_cast<std::uint8_t>(p_value["clouds"].GetInt())
        };

        return data;
    }

    /****************************************************************************************/

    static Data GetDataDaily(const rapidjson::Value& p_value)
    {
        Data data = {
            Date(p_value["sunrise"].GetUint64()),
            Date(p_value["sunset"].GetUint64()),
            p_value["temp"]["day"].GetFloat(),
            p_value["feels_like"]["day"].GetFloat(),
            static_cast<std::uint8_t>(p_value["humidity"].GetInt()),
            static_cast<std::uint8_t>(p_value["clouds"].GetInt())
        };

        return data;
    }

    /****************************************************************************************/

    std::string ForecastStatusToString(const ForecastStatus p_forecastStatus)
    {
        switch (p_forecastStatus)
        {
        case ForecastStatus::ForecastStatus_LocationNotFound:
            return "ForecastStatus_LocationNotFound";

        case ForecastStatus::ForecastStatus_FailedToGetLocation:
            return "ForecastStatus_FailedToGetLocation";

        case ForecastStatus::ForecastStatus_FailedToGetForecast:
            return "ForecastStatus_FailedToGetForecast";

        case ForecastStatus::ForecastStatus_NotValid:
            return "ForecastStatus_NotValid";
        
        case ForecastStatus::ForecastStatus_Undefined:
        default:
            return "ForecastStatus_Undefined";
        }   
    }

    /****************************************************************************************/

    Forecast::Forecast(const ForecastCreateInfo& p_forecastCreateInfo)
    {
        std::stringstream locationAPI, forecastAPI;

        locationAPI << "http://api.openweathermap.org/geo/1.0/direct?q=";
        locationAPI << p_forecastCreateInfo.m_location;
        locationAPI << "&limit=1&appid=18ce38c594d4c49c051172e227b13f13";

        GetterCreateInfo getterCreateInfo = {};

        getterCreateInfo.m_address = locationAPI.str();
        Getter getLocation(getterCreateInfo);

        float latitude, longitude;

        getLocation.GetRessource()
            .map([this, &latitude, &longitude](const Ressource& p_ressource){
                rapidjson::Document json = p_ressource.ToJson();

                if(json.Size() == 0)
                {
                    m_valid = Unexpected<ForecastStatus>(ForecastStatus::ForecastStatus_LocationNotFound);
                    return;
                }

                latitude = json[0]["lat"].GetFloat();
                longitude = json[0]["lon"].GetFloat();
            })
            .or_else([this](const GetterStatus p_getterStatus){
                LOG_ERROR(GetterStatusToString(p_getterStatus));

                m_valid = Unexpected<ForecastStatus>(ForecastStatus::ForecastStatus_FailedToGetLocation);
            });
        if(!m_valid)
        {
            return;
        }

        forecastAPI << "https://api.openweathermap.org/data/2.5/onecall?lat=";
        forecastAPI << std::to_string(latitude);
        forecastAPI << "&lon=";
        forecastAPI << std::to_string(longitude);
        forecastAPI << "&units=metric&exclude=minutely,alerts&appid=18ce38c594d4c49c051172e227b13f13";

        getterCreateInfo.m_address = forecastAPI.str();
        Getter getForecast(getterCreateInfo);

        getForecast.GetRessource()
            .map([this](const Ressource& p_ressource){
                rapidjson::Document json = p_ressource.ToJson();

                m_current = GetDataCurrent(json["current"]);

                auto beginHourly = json["hourly"].GetArray().begin() + 1;
                std::transform(beginHourly, beginHourly + m_hourly.size(), m_hourly.begin(), [](const rapidjson::Value& p_value){
                    return GetDataHourly(p_value);
                });

                auto beginDaily = json["daily"].GetArray().begin() + 1;
                std::transform(beginDaily, beginDaily + m_daily.size(), m_daily.begin(), [](const rapidjson::Value& p_value){
                    return GetDataDaily(p_value);
                });
            })
            .or_else([this](const GetterStatus p_getterStatus){
                LOG_ERROR(GetterStatusToString(p_getterStatus));

                m_valid = Unexpected<ForecastStatus>(ForecastStatus::ForecastStatus_FailedToGetForecast);
            });
    }

    /****************************************************************************************/

    Expected<Data, ForecastStatus> Forecast::GetCurrent() const
    {
        if(!m_valid)
        {
            return Unexpected<ForecastStatus>(ForecastStatus::ForecastStatus_NotValid);
        }

        return m_current;
    }

    /****************************************************************************************/

    Expected<std::array<Data, MAX_FORECAST>, ForecastStatus> Forecast::GetHourly() const
    {
        if(!m_valid)
        {
            return Unexpected<ForecastStatus>(ForecastStatus::ForecastStatus_NotValid);
        }

        return m_hourly;
    }

    /****************************************************************************************/

    Expected<std::array<Data, MAX_FORECAST>, ForecastStatus> Forecast::GetDaily() const
    {
        if(!m_valid)
        {
            return Unexpected<ForecastStatus>(ForecastStatus::ForecastStatus_NotValid);
        }

        return m_daily;
    }

    /****************************************************************************************/

    Valid<ForecastStatus> Forecast::GetValid() const
    {
        return m_valid;
    }
}