/**
 * @file Getter.h
 * @author Romain BOULLARD 
 * @date 2021-11-20
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include "Core/Web/Ressource/Ressource.h"

#include "Utils/Expected/Expected.h"

#include <string>
#include <vector>

namespace Weather
{
    /**
     * Status that can be returned by Getter methods.
     */
    enum class GetterStatus
    {
        GetterStatus_Undefined = 1,         //Undefined status.
        GetterStatus_FailedToGetRessource,  //Failed to get the web ressource.
        GetterStatus_NotValid               //Getter not valid.
    };

    /****************************************************************************************/

    /**
     * Convert a GetterStatus to a human readable string.
     * 
     * @param p_getterStatus The GetterStatus to convert.
     * @return std::string The GetterStatus as a human readable string.
     */
    std::string GetterStatusToString(const GetterStatus p_getterStatus);

    /****************************************************************************************/

    /**
     * The Getter create info.
     */
    struct GetterCreateInfo
    {
        std::string m_address;  //The web address to get.
    };

    /****************************************************************************************/

    /**
     * A web getter.
     */
    class Getter
    {
        public:
            /**
             * Constructor
             * 
             * @param p_getterCreateInfo The Getter create info.
             */
            Getter(const GetterCreateInfo& p_getterCreateInfo);

            /**
             * Destructor.
             */
            ~Getter() = default;

            /**
             * Get the validity status of the Getter.
             * 
             * @return Valid<GetterStatus> The validity status of the Getter.
             */
            Valid<GetterStatus> GetValid() const;

            /**
             * Get the Ressource of the Getter.
             * 
             * @return Expected<Ressource, GetterStatus> The Ressource of the Getter.
             */
            Expected<Ressource, GetterStatus> GetRessource() const;

            /**
             * Initialize CURL
             */
            static void InitCurl();

            /**
             * Clean CURL
             */
            static void CleanCurl();
        private:
            /**
             * Is the Getter valid?
             */
            Valid<GetterStatus> m_valid;

            /**
             * The ressource.
             */
            Ressource m_ressource;
    };
}