/**
 * @file Getter.cpp
 * @author Romain BOULLARD 
 * @date 2021-11-20
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "Core/Web/Getter/Getter.h"

#include "Debug/Log/Log.h"

#include "curl/curl.h"

#include <algorithm>

namespace Weather
{
    std::string GetterStatusToString(const GetterStatus p_getterStatus)
    {
        switch(p_getterStatus)
        {
            case GetterStatus::GetterStatus_FailedToGetRessource:
                return "GetterStatus_FailedToGetRessource";

            case GetterStatus::GetterStatus_NotValid:
                return "GetterStatus_NotValid";

            case GetterStatus::GetterStatus_Undefined:
            default:
                return "GetterStatus_Undefined";
        }
    }

    /****************************************************************************************/

    static std::size_t WriteRessource(void* p_content, std::size_t p_size, std::size_t p_nmemb, void* p_ressource)
    {
        std::size_t totalSizeToWrite = p_size * p_nmemb;
        Ressource* ressource = reinterpret_cast<Ressource*>(p_ressource);

        ressource->m_raw.reserve(ressource->m_raw.size() + totalSizeToWrite);

        LOG_INFO("Writing {0} bytes", totalSizeToWrite);

        // I hesitated between memcpy and std::copy
        // https://stackoverflow.com/questions/4707012/is-it-better-to-use-stdmemcpy-or-stdcopy-in-terms-to-performance
        std::copy(static_cast<std::byte*>(p_content), static_cast<std::byte*>(p_content) + totalSizeToWrite, std::back_inserter(ressource->m_raw));

        return totalSizeToWrite;
    }

    /****************************************************************************************/

    Getter::Getter(const GetterCreateInfo& p_getterCreateInfo)
    {
        LOG_INFO("Getting web resource: {0}", p_getterCreateInfo.m_address);

        CURLcode curlResult;
        std::unique_ptr<CURL, std::function<void(CURL*)>> handle = std::unique_ptr<CURL, std::function<void(CURL*)>>(curl_easy_init(), curl_easy_cleanup);

        curl_easy_setopt(handle.get(), CURLOPT_URL, p_getterCreateInfo.m_address.c_str());

        curl_easy_setopt(handle.get(), CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(handle.get(), CURLOPT_SSL_VERIFYHOST, 0L);

        curl_easy_setopt(handle.get(), CURLOPT_WRITEFUNCTION, WriteRessource);
        curl_easy_setopt(handle.get(), CURLOPT_WRITEDATA, static_cast<void*>(&m_ressource));

        curlResult = curl_easy_perform(handle.get());

        // TODO: Create mor meaningful status errors.
        if (curlResult != CURLE_OK)
        {
            LOG_ERROR("CURL error: {0}", curl_easy_strerror(curlResult));
            m_valid = Unexpected<GetterStatus>(GetterStatus::GetterStatus_FailedToGetRessource);
            return;
        }
    }

    /****************************************************************************************/

    Valid<GetterStatus> Getter::GetValid() const
    {
        return m_valid;
    }

    /****************************************************************************************/

    Expected<Ressource, GetterStatus> Getter::GetRessource() const
    {
        if(!m_valid)
        {
            return Unexpected<GetterStatus>(GetterStatus::GetterStatus_NotValid);
        }

        return m_ressource;
    }

    /****************************************************************************************/

    void Getter::InitCurl()
    {
        curl_global_init(CURL_GLOBAL_DEFAULT);
    }

    /****************************************************************************************/

    void Getter::CleanCurl()
    {
        curl_global_cleanup();
    }
}