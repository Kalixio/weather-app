/**
 * @file Ressource.h
 * @author Romain BOULLARD 
 * @date 2021-11-20
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include "rapidjson/document.h"

#include <vector>

namespace Weather
{
    /**
     * A Web Ressource.
     */
    struct Ressource
    {
        /**
         * Raw data.
         */
        std::vector<std::byte> m_raw;

        /**
         * Jsonify m_raw.
         * 
         * @return rapidjson::Document The Ressource converted to a Json exploitable format.
         */
        rapidjson::Document ToJson() const;
    };
}