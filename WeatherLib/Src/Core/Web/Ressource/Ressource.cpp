/**
 * @file Ressource.cpp
 * @author Romain BOULLARD 
 * @date 2021-11-20
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "Core/Web/Ressource/Ressource.h"

#include <string>

namespace Weather
{
    rapidjson::Document Ressource::ToJson() const
    {
        //TODO: Dangerous, we don't handle errors here. Can it fail?
        rapidjson::Document document;
        document.Parse(std::string(reinterpret_cast<const char *>(&m_raw.data()[0]), m_raw.size()).c_str());

        return document;
    }
}