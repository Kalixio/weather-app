/**
 * @file WeatherLib.h
 * @author Romain BOULLARD 
 * @date 2021-11-21
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once

#include "Core/Forecast/Forecast.h"
#include "Core/Web/Getter/Getter.h"

#include "Debug/Log/Log.h"
