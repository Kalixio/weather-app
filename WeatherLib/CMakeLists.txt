cmake_minimum_required(VERSION 3.15)

project(WeatherLib VERSION 0.0.1
                DESCRIPTION "A Weather Library"
                LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

file(GLOB_RECURSE WEATHER_LIB_SRC
    ${CMAKE_CURRENT_SOURCE_DIR}/Src/*.h         ${CMAKE_CURRENT_SOURCE_DIR}/Src/*.cpp
)
add_library(${PROJECT_NAME} ${WEATHER_LIB_SRC})

target_include_directories(${PROJECT_NAME} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/Src)

# TODO: Yurk...
if(TARGET CONAN_PKG::CURL)
    target_link_libraries(${PROJECT_NAME} PRIVATE CONAN_PKG::CURL)
elseif(TARGET CONAN_PKG::libcurl)
    target_link_libraries(${PROJECT_NAME} PRIVATE CONAN_PKG::libcurl)
endif()
target_link_libraries(${PROJECT_NAME} PUBLIC CONAN_PKG::tl-expected)
target_link_libraries(${PROJECT_NAME} PUBLIC CONAN_PKG::rapidjson)
target_link_libraries(${PROJECT_NAME} PUBLIC CONAN_PKG::spdlog)

target_compile_definitions(${PROJECT_NAME}
                            PUBLIC $<$<CONFIG:Debug>:WEATHER_DEBUG>
                            PUBLIC $<$<CONFIG:Release>:WEATHER_RELEASE>
                            PUBLIC $<$<CONFIG:RelWithDebInfo>:WEATHER_RELEASE_WITH_DEBUG_INFO>
                            PUBLIC $<$<CONFIG:MinSizeRel>:WEATHER_RELEASE>)

source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR}/Src PREFIX Src FILES ${TESTS_SRC})