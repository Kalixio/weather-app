[generators]
cmake_multi

[requires]
libcurl/7.79.1
tl-expected/20190710
rapidjson/cci.20200410
gtest/1.11.0
spdlog/1.9.2