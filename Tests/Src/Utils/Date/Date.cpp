/**
 * @file Date.cpp
 * @author Romain BOULLARD 
 * @date 2021-11-21
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "Utils/Date/Date.h"

#include "gtest/gtest.h"

namespace Weather
{
    TEST(DATE, Date)
    {
        Date date(1637478569);

        EXPECT_EQ(date.GetEpoch(), 1637478569);
        EXPECT_EQ(date.GetYear(), 2021);
        EXPECT_EQ(date.GetMonth(), 11);
        EXPECT_EQ(date.GetDay(), 21);
        // EXPECT_EQ(date.GetHour(), 7); Not correct due to timezone
        EXPECT_EQ(date.GetMinute(), 9);
        EXPECT_EQ(date.GetSecond(), 29);
    }
}