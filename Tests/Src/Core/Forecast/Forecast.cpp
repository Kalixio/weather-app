/**
 * @file Forecast.cpp
 * @author Romain BOULLARD 
 * @date 2021-11-21
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "Core/Forecast/Forecast.h"

#include "gtest/gtest.h"

#include <algorithm>

namespace Weather
{
    TEST(FORECAST, ForecastStatusToString)
    {
        EXPECT_STREQ(ForecastStatusToString(ForecastStatus::ForecastStatus_LocationNotFound).c_str(), "ForecastStatus_LocationNotFound");
        EXPECT_STREQ(ForecastStatusToString(ForecastStatus::ForecastStatus_FailedToGetLocation).c_str(), "ForecastStatus_FailedToGetLocation");
        EXPECT_STREQ(ForecastStatusToString(ForecastStatus::ForecastStatus_FailedToGetForecast).c_str(), "ForecastStatus_FailedToGetForecast");
        EXPECT_STREQ(ForecastStatusToString(ForecastStatus::ForecastStatus_NotValid).c_str(), "ForecastStatus_NotValid");
        EXPECT_STREQ(ForecastStatusToString(ForecastStatus::ForecastStatus_Undefined).c_str(), "ForecastStatus_Undefined");
    }

    /****************************************************************************************/

    static Forecast chaville(ForecastCreateInfo({"Chaville"}));
    static Forecast notValid(ForecastCreateInfo({"uhsbdffuherbfyu"}));

    /****************************************************************************************/

    TEST(FORECAST, GetValid)
    {
        EXPECT_TRUE(chaville.GetValid());

        EXPECT_FALSE(notValid.GetValid());
        EXPECT_EQ(notValid.GetValid().error(), ForecastStatus::ForecastStatus_LocationNotFound);
    }

    /****************************************************************************************/

    TEST(FORECAST, GetCurrent)
    {
        Expected<Data, ForecastStatus> current;

        current = chaville.GetCurrent();
        EXPECT_TRUE(current);

        EXPECT_TRUE(current.value().m_sunrise.has_value() && current.value().m_sunset.has_value());

        current = notValid.GetCurrent();
        EXPECT_FALSE(current);
        EXPECT_EQ(current.error(), ForecastStatus::ForecastStatus_NotValid);
    }

    /****************************************************************************************/

    TEST(FORECAST, GetHourly)
    {
        Expected<std::array<Data, MAX_FORECAST>, ForecastStatus> hourly;

        hourly = chaville.GetHourly();
        EXPECT_TRUE(hourly);

        EXPECT_FALSE(std::all_of(hourly.value().begin(), hourly.value().end(), [](const Data& p_data){
            return p_data.m_sunset.has_value() && p_data.m_sunrise.has_value();
        }));

        hourly = notValid.GetHourly();
        EXPECT_FALSE(hourly);
        EXPECT_EQ(hourly.error(), ForecastStatus::ForecastStatus_NotValid);
    }

    /****************************************************************************************/

    TEST(FORECAST, GetDaily)
    {
        Expected<std::array<Data, MAX_FORECAST>, ForecastStatus> daily;

        daily = chaville.GetDaily();
        EXPECT_TRUE(daily);

        EXPECT_TRUE(std::all_of(daily.value().begin(), daily.value().end(), [](const Data& p_data){
            return p_data.m_sunset.has_value() && p_data.m_sunrise.has_value();
        }));

        daily = notValid.GetDaily();
        EXPECT_FALSE(daily);
        EXPECT_EQ(daily.error(), ForecastStatus::ForecastStatus_NotValid);
    }
}