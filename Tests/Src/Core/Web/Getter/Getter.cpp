/**
 * @file Getter.cpp
 * @author Romain BOULLARD 
 * @date 2021-11-20
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "Core/Web/Getter/Getter.h"

#include "gtest/gtest.h"

namespace Weather
{
    TEST(GETTER, GetterStatusToString)
    {
        EXPECT_STREQ(GetterStatusToString(GetterStatus::GetterStatus_FailedToGetRessource).c_str(), "GetterStatus_FailedToGetRessource");
        EXPECT_STREQ(GetterStatusToString(GetterStatus::GetterStatus_NotValid).c_str(), "GetterStatus_NotValid");
        EXPECT_STREQ(GetterStatusToString(GetterStatus::GetterStatus_Undefined).c_str(), "GetterStatus_Undefined");
    }

    /****************************************************************************************/

    TEST(GETTER, Init)
    {
        Getter::InitCurl();
    }

    /****************************************************************************************/

    static Getter chaville(GetterCreateInfo({"http://api.openweathermap.org/geo/1.0/direct?q=Chaville&limit=5&appid=18ce38c594d4c49c051172e227b13f13"}));
    static Getter notValid(GetterCreateInfo({"blabla"}));

    /****************************************************************************************/

    TEST(GETTER, GetValid)
    {
        EXPECT_TRUE(chaville.GetValid());

        EXPECT_FALSE(notValid.GetValid());
        EXPECT_EQ(notValid.GetValid().error(), GetterStatus::GetterStatus_FailedToGetRessource);
    }

    /****************************************************************************************/

    TEST(GETTER, GetRessource)
    {
        Expected<Ressource, GetterStatus> ressource;

        ressource = chaville.GetRessource();
        EXPECT_TRUE(ressource);
        // TODO: Might fail if json result changes a little bit
        //EXPECT_STREQ(
        //    std::string(reinterpret_cast<const char *>(&ressource.value().m_raw.data()[0]), ressource.value().m_raw.size()).c_str(), 
        //    "[{\"name\":\"Chaville\",\"local_names\":{\"ascii\":\"Chaville\",\"de\":\"Chaville\",\"en\":\"Chaville\",\"feature_name\":\"Chaville\",\"fr\":\"Chaville\",\"it\":\"Chaville\",\"ja\":\"シャヴィル\",\"la\":\"Cativilla\",\"nl\":\"Chaville\",\"pl\":\"Chaville\",\"ro\":\"Chaville\",\"ru\":\"Шавиль\",\"sr\":\"Шавил\"},\"lat\":48.8056,\"lon\":2.1886,\"country\":\"FR\"},{\"name\":\"Chaville\",\"local_names\":{\"ascii\":\"Chaville\",\"feature_name\":\"Chaville\",\"fr\":\"Chaville\"},\"lat\":48.8,\"lon\":2.2,\"country\":\"FR\"}]"
        //    );

        ressource = notValid.GetRessource();
        EXPECT_FALSE(ressource);
        EXPECT_EQ(ressource.error(), GetterStatus::GetterStatus_NotValid);
    }

    /****************************************************************************************/

    TEST(GETTER, Clean)
    {
        Getter::CleanCurl();
    }
}