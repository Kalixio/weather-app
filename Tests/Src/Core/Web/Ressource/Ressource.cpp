/**
 * @file Ressource.cpp
 * @author Romain BOULLARD 
 * @date 2021-11-20
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "Core/Web/Ressource/Ressource.h"

#include "gtest/gtest.h"

namespace Weather
{
    TEST(RESSOURCE, ToJson)
    {
        std::string jsonSource = "[{\"name\":\"Chaville\",\"local_names\":{\"ascii\":\"Chaville\",\"de\":\"Chaville\",\"en\":\"Chaville\",\"feature_name\":\"Chaville\",\"fr\":\"Chaville\",\"it\":\"Chaville\",\"ja\":\"シャヴィル\",\"la\":\"Cativilla\",\"nl\":\"Chaville\",\"pl\":\"Chaville\",\"ro\":\"Chaville\",\"ru\":\"Шавиль\",\"sr\":\"Шавил\"},\"lat\":48.8056,\"lon\":2.1886,\"country\":\"FR\"},{\"name\":\"Chaville\",\"local_names\":{\"ascii\":\"Chaville\",\"feature_name\":\"Chaville\",\"fr\":\"Chaville\"},\"lat\":48.8,\"lon\":2.2,\"country\":\"FR\"}]";
        Ressource ressource;
        ressource.m_raw = std::vector<std::byte>(reinterpret_cast<const std::byte*>(jsonSource.c_str()), reinterpret_cast<const std::byte*>(jsonSource.c_str()) + jsonSource.size());

        EXPECT_STREQ(ressource.ToJson()[0]["name"].GetString(), "Chaville");
        EXPECT_FLOAT_EQ(ressource.ToJson()[0]["lat"].GetFloat(), 48.8056);
        EXPECT_FLOAT_EQ(ressource.ToJson()[0]["lon"].GetFloat(), 2.1886);
        EXPECT_STREQ(ressource.ToJson()[0]["country"].GetString(), "FR");
    }
}