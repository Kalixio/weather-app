/**
 * @file WeatherApp.cpp
 * @author Romain BOULLARD 
 * @date 2021-11-21
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "WeatherLib.h"

#include <iostream>
#include <algorithm>

enum class Arg
{
    Arg_Undefined = 1,
    Arg_Current,
    Arg_Hourly,
    Arg_Daily
};

/****************************************************************************************/

static void DisplayData(const Weather::Data& p_data)
{
    std::cout << "Temperature: " << p_data.m_temp << " degrees" << std::endl;
    std::cout << "Temperature feels: " << p_data.m_feels << " degrees" << std::endl;
    std::cout << "Humidity: " << static_cast<std::uint16_t>(p_data.m_humidity) << "%" << std::endl;
    std::cout << "Clouds coverage: " << static_cast<std::uint16_t>(p_data.m_clouds) << "%" << std::endl;

    if(p_data.m_sunrise.has_value() && p_data.m_sunset.has_value())
    {
        Weather::Date sunrise = p_data.m_sunrise.value();
        Weather::Date sunset = p_data.m_sunset.value();

        std::cout << "Sunrise: " << static_cast<std::uint16_t>(sunrise.GetHour()) << "H" << static_cast<std::uint16_t>(sunrise.GetMinute()) << "M" << static_cast<std::uint16_t>(sunrise.GetSecond()) << "S" << std::endl;
        std::cout << "Sunset: " << static_cast<std::uint16_t>(sunset.GetHour()) << "H" << static_cast<std::uint16_t>(sunset.GetMinute()) << "M" << static_cast<std::uint16_t>(sunset.GetSecond()) << "S" << std::endl;
    }
}

/****************************************************************************************/

static Arg CheckArg(const std::string& p_arg)
{
    if(p_arg == "--current")
    {
        return Arg::Arg_Current;
    }

    if(p_arg == "--hourly")
    {
        return Arg::Arg_Hourly;
    }

    if(p_arg == "--daily")
    {
        return Arg::Arg_Daily;
    }

    return Arg::Arg_Undefined;
}

/****************************************************************************************/

static void ShowUsage()
{
    std::cout << "PROGRAM USAGE" << std::endl;
    std::cout << "First argument: location (ex: ./WeatherApp Chaville)" << std::endl;
    std::cout << "Optional parameters: --current, --hourly, --daily (ex: ./WeatherApp Chaville --hourly)" << std::endl;
}

/****************************************************************************************/

static void ShowForecastCurrent(const Weather::Forecast& p_forecast, const std::string& p_location)
{
    p_forecast.GetCurrent()
        .map([&p_location](const Weather::Data& p_current){
            std::cout << "Currently at " << p_location << ":" << std::endl;
            DisplayData(p_current);
            std::cout << std::endl;
        })
        .or_else([&p_forecast](const Weather::ForecastStatus p_forecastStatus){
            LOG_ERROR(Weather::ForecastStatusToString(p_forecastStatus));
            LOG_ERROR(Weather::ForecastStatusToString(p_forecast.GetValid().error()));
        });
}

/****************************************************************************************/

static void ShowForecastHourly(const Weather::Forecast& p_forecast, const std::string& p_location)
{
    std::cout << "***********************HOURLY FORECAST***********************" << std::endl;

    p_forecast.GetHourly()
        .map([&p_location](const std::array<Weather::Data, MAX_FORECAST>& p_hourly){
            std::size_t hour = 1;

            std::for_each(p_hourly.begin(), p_hourly.end(), [&p_location, &hour](const Weather::Data p_hour) {
                std::cout << "In " << hour << " hours  at " << p_location << ":" << std::endl;
                DisplayData(p_hour);
                hour++;
                std::cout << std::endl;
            });
        })
        .or_else([&p_forecast](const Weather::ForecastStatus p_forecastStatus){
            LOG_ERROR(Weather::ForecastStatusToString(p_forecastStatus));
            LOG_ERROR(Weather::ForecastStatusToString(p_forecast.GetValid().error()));
        });
}

/****************************************************************************************/

static void ShowForecastDaily(const Weather::Forecast& p_forecast, const std::string& p_location)
{
    std::cout << "***********************DAILY FORECAST***********************" << std::endl;

    p_forecast.GetDaily()
        .map([&p_location](const std::array<Weather::Data, MAX_FORECAST>& p_daily){
            std::size_t day = 1;

            std::for_each(p_daily.begin(), p_daily.end(), [&p_location, &day](const Weather::Data p_day) {
                std::cout << "In " << day << " days  at " << p_location << ":" << std::endl;
                DisplayData(p_day);
                day++;
                std::cout << std::endl;
            });
        })
        .or_else([&p_forecast](const Weather::ForecastStatus p_forecastStatus){
            LOG_ERROR(Weather::ForecastStatusToString(p_forecastStatus));
            LOG_ERROR(Weather::ForecastStatusToString(p_forecast.GetValid().error()));
        });
}

/****************************************************************************************/

int main(int argc, char** argv)
{
    bool current = true, hourly = true, daily = true;

    if(argc < 2)
    {
        ShowUsage();
        return 1;
    }

    if(argc > 2)
    {
        current = false;
        hourly = false;
        daily = false;

        for(std::size_t i = 2; i < argc; ++i)
        {
            Arg arg = CheckArg(argv[i]);

            switch(arg)
            {
                case Arg::Arg_Current:
                    current = true;
                    break;
                
                case Arg::Arg_Daily:
                    daily = true;
                    break;

                case Arg::Arg_Hourly:
                    hourly = true;
                    break;

                default:
                    ShowUsage();
                    return 1;
            }
        }
    }

    LOG_INFO("Getting the weather for {0}", argv[1]);

    Weather::Getter::InitCurl();

    Weather::ForecastCreateInfo forecastCreateInfo({argv[1]});
    Weather::Forecast forecast(forecastCreateInfo);

    if(current)
    {
        ShowForecastCurrent(forecast, argv[1]);
    }

    if(hourly)
    {
        ShowForecastHourly(forecast, argv[1]);
    }

    if(daily)
    {
        ShowForecastDaily(forecast, argv[1]);
    }

    Weather::Getter::CleanCurl();
    
    return 0;
}