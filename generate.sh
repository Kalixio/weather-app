#!/bin/sh 

if [ "$#" -eq 0 ]; then
    echo "Script usage : ./generate.sh --debug --release --minsizerel --relwithdebinfo"
    exit
fi

if [ ! -d "build" ]; then
    mkdir build
fi

debug=false
release=false
relwithdebinfo=false
minsizerel=false
cmakebuildtype=Debug

for var in "$@"
do
    if [ "$var" = "--debug" ]; then
        debug=true
    elif [ "$var" = "--release" ]; then
        release=true
    elif [ "$var" = "--relwithdebinfo" ]; then
        relwithdebinfo=true
    elif [ "$var" = "--minsizerel" ]; then
        minsizerel=true
    else
        echo "Script usage : ./generate.sh --debug --release --minsizerel --relwithdebinfo"
        exit
    fi
done

if [ $debug = true ]; then
    conan install . --build=missing --no-imports -if build -s build_type=Debug
    cmakebuildtype=Debug
fi

if [ $release = true ]; then
    conan install . --build=missing --no-imports -if build -s build_type=Release
    cmakebuildtype=Release
fi

if [ $relwithdebinfo = true ]; then
    conan install . --build=missing --no-imports -if build -s build_type=MinSizeRel
    cmakebuildtype=RelWithDebInfo
fi

if [ $minsizerel = true ]; then
    conan install . --build=missing --no-imports -if build -s build_type=RelWithDebInfo
    cmakebuildtype=MinSizeRel
fi

conan imports . -if build/ -imf .

cd build

cmake .. -DBUILD_TESTS=ON -DCMAKE_BUILD_TYPE=$cmakebuildtype
