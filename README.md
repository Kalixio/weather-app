Generaten the project with the provided generate.sh script.

You will need Conan and CMake to compile. Be sure to have them installed.

Known issue: Sometimes CMake will complain about not finding CURL or GTest. Not sure where this is coming from. Simply replace CURL by libcurl and GTest by gtest in the corresponding CMake file

Known issue: The console output is ugly, not enough feedback when errors

Known issue:  Not handle locations with spaces (example: Le Lion d'Or)
